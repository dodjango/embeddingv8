# embedding v8 JavaScript engine in C++ console application

## how to use the embedded v8 JavaScript engine

![Sample Video](video.mp4)

## web links

- [V8 Sample Source Code](https://chromium.googlesource.com/v8/v8/+/branch-heads/6.8/samples/shell.cc)
- [Getting started embedding v8](https://v8.dev/docs/embed)
- [NuGet Package on GitHub](https://github.com/pmed/v8-nuget)
